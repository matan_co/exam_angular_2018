// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  url: 'http://localhost:8080',
  firebase: {
    apiKey: "AIzaSyC2LRHXkY_l1Wbgc0Zqfb08GhugJSMnfgU",
    authDomain: "exam-efad9.firebaseapp.com",
    databaseURL: "https://exam-efad9.firebaseio.com",
    projectId: "exam-efad9",
    storageBucket: "exam-efad9.appspot.com",
    messagingSenderId: "203002941548"
  }
};
