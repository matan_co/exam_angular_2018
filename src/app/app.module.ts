import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { NgModule } from '@angular/core';
import { environment } from '../environments/environment';

import { AppComponent } from './app.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { NavigationComponent } from './navigation/navigation.component';
import { LoginComponent } from './login/login.component';
import { ProductsComponent } from './products/products.component';
import { ProductsService } from './products/products.service';
import { SearchResultsComponent } from './products/search-results/search-results.component';
import { EditProductComponent } from './products/edit-product/edit-product.component';
import { FproductsComponent } from './products/fproducts/fproducts.component';

const appRoutes: Routes = [
  { path: ''                                     , component: ProductsComponent       },
  { path: 'edit-product/:id'                     , component: EditProductComponent    },
  { path: 'search-results/:search_term'          , component: SearchResultsComponent  },
  { path: 'login'                                , component: LoginComponent          },
  { path: 'products'                             , component: ProductsComponent       },
  { path: 'fproducts'                            , component: FproductsComponent      },
  { path: '**'                                   , component: NotFoundComponent       }  
];

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,
    NavigationComponent,
    LoginComponent,
    ProductsComponent,
    SearchResultsComponent,
    EditProductComponent,
    FproductsComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(      
      appRoutes,
      { enableTracing: false }
    )
  ],
  providers: [ProductsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
